#include "client.h"
#include <iostream>

Client::Client() { }

Client::~Client() { }

void Client::on_server_something(bool a, int b) {
  std::cout << "Client got something:\n"
            << "\tbool = " << a << "\n"
            << "\tint  = " << b << "\n";
}
