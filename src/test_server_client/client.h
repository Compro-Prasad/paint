#ifndef CLIENT_H
#define CLIENT_H

#include <sigc++/sigc++.h>

class Client : public sigc::trackable {
public:
  Client();
  ~Client();

  void on_server_something(bool, int);
};

#endif // CLIENT_H
