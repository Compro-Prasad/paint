#ifndef SERVER_H
#define SERVER_H

#include <sigc++/sigc++.h>

class Server {
public:
  Server();
  ~Server();

  typedef sigc::signal<void, bool, int> some_signal_t;
  some_signal_t signal_something();

  void do_something();

protected:
  some_signal_t a_var;
};

#endif // SERVER_H
