#include "client.h"
#include "server.h"

#include <iostream>

int main() {
  Server s;
  Client c;

  s.signal_something().connect(sigc::mem_fun(c, &Client::on_server_something));

  std::cout << "Before Server::do_something\n";

  // To get input use `C-u M-x compile`
  int x;
  std::cin >> x;
  std::cout << "I got " << x << "\n";

  s.do_something();

  std::cout << "After Server::do_something\n";

  return 0;
}
