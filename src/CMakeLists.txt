# Use pkg_config to get library and include paths
find_package(PkgConfig REQUIRED)

# compile with c++14 standard
set(CMAKE_CXX_STANDARD 14)

# The target executable name
set(TARGET paint)

# Search for Gtkmm and Cairomm
pkg_search_module(CAIROMM REQUIRED cairomm-1.0)
pkg_search_module(GTKMM REQUIRED gtkmm-3.0)

# I want this to generate compile_commands.json which is use by completion
# servers to provide completion and other stuff
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# Set some variables for Gresource
set (RESOURCE_DIR ${CMAKE_SOURCE_DIR}/src)
set (GRESOURCE_XML ${RESOURCE_DIR}/interfaces.gresources.xml)
set (GRESOURCE_C interfaces.gresources.c)

# Add a command to generate resource source file because cmake doesn't knows
# about how to do this
add_custom_command(OUTPUT ${GRESOURCE_C}
  COMMAND glib-compile-resources --target ${GRESOURCE_C} --generate-source --sourcedir ${RESOURCE_DIR} ${GRESOURCE_XML}
  MAIN_DEPENDENCY ${GRESOURCE_XML}
  DEPENDS
  ${RESOURCE_DIR}/paint-ui.glade
  COMMENT "Generating ${GRESOURCE_C}")

# Executable will be in building directory and not any other sub directory
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/)

# Set variable to point to libraries
set(LIBS ${LIBS} ${CAIROMM_LIBRARIES} ${GTKMM_LIBRARIES})

# Includes the directories containing the header files
include_directories(${CAIROMM_INCLUDE_DIRS})
include_directories(${GTKMM_INCLUDE_DIRS})

# Creates the executable. If ${GRESOURCE_C} isn't there or the xml has been
# updated then ${GRESOURCE_C} will be built first
add_executable(${TARGET} main.cpp ${GRESOURCE_C})

# Links the target to the libraries
target_link_libraries(${TARGET} ${LIBS})
