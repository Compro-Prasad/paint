#include <iostream>
#include <cairomm/enums.h>
#include <cairomm/context.h>
#include <gtkmm/button.h>
#include <gtkmm/main.h>
#include <gtkmm/window.h>
#include <gtkmm/applicationwindow.h>
#include <gtkmm/drawingarea.h>
#include <gtkmm/builder.h>
#include <gtkmm/box.h>
#include <gtkmm.h>


int main(int argc, char **argv)
{
  auto app = Gtk::Application::create(argc, argv, "org.gtkmm.example");
  Gtk::DrawingArea *paintarea;
  Gtk::ApplicationWindow *mainwindow;

  //Load the GtkBuilder file and instantiate its widgets:
  auto source_ui = Gtk::Builder::create_from_resource("/main/paint-ui.glade");
  source_ui->get_widget("paintarea", paintarea);
  source_ui->get_widget("mainwindow", mainwindow);
  if (!app or !mainwindow or !paintarea)
    {
      std::cout << "Failed to get_widget";
      return 1;
    }
  paintarea->signal_draw()
    .connect([&paintarea](const Cairo::RefPtr<Cairo::Context> cr) -> int {

               int width  = paintarea->get_allocation().get_width();
               int height = paintarea->get_allocation().get_height();

               /* clear background */
               cr->set_source_rgb (1,1,1);
               cr->paint ();


               cr->select_font_face ("Sans",
                                     Cairo::FONT_SLANT_NORMAL,
                                     Cairo::FONT_WEIGHT_BOLD);

               /* enclosing in a save/restore pair since we alter the
                * font size */
               cr->save ();
               cr->set_font_size (40);
               cr->move_to (40, 60);
               cr->set_source_rgb (0,0,0);
               cr->show_text ("Hello World");
               cr->restore ();

               cr->set_source_rgb (1,0,0);
               cr->set_font_size (20);
               cr->move_to (50, 100);
               cr->show_text ("greetings from gtk and cairo");

               cr->set_source_rgb (0,0,1);

               cr->move_to (0, 150);
               for (int i=0; i< width/10; i++)
                 {
                   cr->rel_line_to (5,  10);
                   cr->rel_line_to (5, -10);
                 }
               cr->stroke ();

               return 0;
             });

  app->run(*mainwindow);
  return 0;
}
